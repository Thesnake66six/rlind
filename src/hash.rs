use std::hash::{BuildHasher, Hasher};

#[derive(Default, Clone, Copy)]
pub struct CustomHasher(u64);

impl Hasher for CustomHasher {
    fn finish(&self) -> u64 {
        self.0
    }

    fn write(&mut self, bytes: &[u8]) {
        self.0 += bytes.iter().map(|x| *x as u64).sum::<u64>();
    }
}

impl BuildHasher for CustomHasher {
    type Hasher = Self;
    fn build_hasher(&self) -> Self::Hasher {
        *self
    }
}
