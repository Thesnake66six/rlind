use raylib::{
    check_collision_lines,
    color::Color,
    core::{
        drawing::RaylibDraw,
        math::{Rectangle, Vector2},
    },
};

#[derive(Clone, Copy, Debug)]
pub struct TurtlePoint {
    pub point: Vector2,
    pub pen_down: bool,
    pub thick: f32,
}

#[derive(Clone)]
pub struct Turtle {
    pub points: Vec<TurtlePoint>,
    pub heading: f32,
    pub pen_down: bool,
    pub color: Color,
    pub thick: f32,
    sum_distance: f32,
}

impl Turtle {
    pub fn new(thick: f32) -> Self {
        Self {
            points: vec![TurtlePoint {
                point: Vector2::zero(),
                pen_down: true,
                thick,
            }],
            heading: 0.0,
            color: Color::BLACK,
            thick,
            pen_down: true,
            sum_distance: 0.0,
        }
    }

    pub fn forward(&mut self, amount: f32) {
        let mut point = *self.points.last().unwrap();
        point.point += Vector2::from(self.heading.to_radians().sin_cos()) * amount;
        point.pen_down = self.pen_down;
        point.thick = self.thick;
        let prev_point = self.points.last().unwrap().point;
        let distance = point.point.distance_to(prev_point);
        self.sum_distance += distance;
        self.points.push(point);
    }

    pub fn goto(&mut self, point: Vector2) {
        self.points.push(TurtlePoint {
            point,
            pen_down: self.pen_down,
            thick: self.thick,
        });
    }

    pub fn get_position(&self) -> Vector2 {
        self.points.last().unwrap().point
    }
    pub fn rotate(&mut self, amount: f32) {
        self.heading += amount
    }

    pub fn points_rect(&self) -> Rectangle {
        let max_x = self
            .points
            .iter()
            .map(|x| x.point.x)
            .max_by(|a, b| a.partial_cmp(b).unwrap())
            .unwrap();
        let max_y = self
            .points
            .iter()
            .map(|x| x.point.y)
            .max_by(|a, b| a.partial_cmp(b).unwrap())
            .unwrap();
        let min_x = self
            .points
            .iter()
            .map(|x| x.point.x)
            .min_by(|a, b| a.partial_cmp(b).unwrap())
            .unwrap();
        let min_y = self
            .points
            .iter()
            .map(|x| x.point.y)
            .min_by(|a, b| a.partial_cmp(b).unwrap())
            .unwrap();

        let box_min_x;
        let box_max_x;

        let box_min_y;
        let box_max_y;

        if max_x - min_x > max_y - min_y {
            let diff_y = max_y - min_y;
            let diff_x = max_x - min_x;

            box_min_x = min_x;
            box_max_x = max_x;

            box_min_y = min_y - ((diff_x - diff_y) / 2.0);
            box_max_y = max_y + ((diff_x - diff_y) / 2.0);
        } else {
            let diff_x = max_x - min_x;
            let diff_y = max_y - min_y;

            box_min_y = min_y;
            box_max_y = max_y;

            box_min_x = min_x - ((diff_y - diff_x) / 2.0);
            box_max_x = max_x + ((diff_y - diff_x) / 2.0);
        };

        Rectangle::new(
            box_min_x,
            box_min_y,
            box_max_x - box_min_x,
            box_max_y - box_min_y,
        )
    }

    pub fn draw_with_limits(
        &mut self,
        min: Vector2,
        max: Vector2,
        zoom: f32,
        draw: &mut impl RaylibDraw,
        rainbow: bool,
        circles: bool,
    ) {
        let rect = Rectangle::new(min.x, min.y, max.x - min.x, max.y - min.y);
        if self.points.len() < 2 {
            return;
        }

        let mut point;
        let mut prev_point = self.points[0];
        let mut sum_distance = 0.0;

        for (i, tpoint) in self.points.iter().enumerate() {
            point = *tpoint;

            // Check to see if we have 'pen down'
            if !point.pen_down {
                prev_point = point;
                continue;
            }

            // Calculate the distance between the two points
            let distance = point.point.distance_to(prev_point.point);

            // Incrememnt the rainbow colours.
            sum_distance += distance;

            // Perform culling.
            let mut intersects = false;
            let ul = min;
            let br = max;
            let ur = Vector2::new(max.x, min.y);
            let bl = Vector2::new(min.x, max.y);
            intersects |= check_collision_lines(ul, ur, point.point, prev_point.point).is_some();
            intersects |= check_collision_lines(ul, bl, point.point, prev_point.point).is_some();
            intersects |= check_collision_lines(ur, br, point.point, prev_point.point).is_some();
            intersects |= check_collision_lines(bl, br, point.point, prev_point.point).is_some();
            intersects |= rect.check_collision_point_rec(point.point);
            intersects |= rect.check_collision_point_rec(prev_point.point);

            if !intersects {
                prev_point = point;
                continue;
            }

            // Mandate that the turtle's lines are at least 1-pixel
            // on screen.
            let thick_end = if point.thick * zoom < 1.0 {
                1.0 / zoom
            } else {
                point.thick
            };

            let thick_start = if prev_point.thick * zoom < 1.0 {
                1.0 / zoom
            } else {
                prev_point.thick
            };

            // Check if the line is too small
            if distance * zoom < 1.0 || distance < thick_start.min(thick_end) {
                sum_distance -= distance;
                continue;
            }

            // Find the rainbow colours (if applicable)
            // to draw with.
            let color = if rainbow {
                Color::color_from_hsv(360.0 * sum_distance / self.sum_distance, 1.0, 1.0)
            } else {
                self.color
            };

            {
                let delta = (point.point - prev_point.point).normalized();
                let length = (delta.x * delta.x + delta.y * delta.y).sqrt();
                let scale_start = thick_start / (2.0 * length);
                let scale_end = thick_end / (2.0 * length);
                let radius_start = Vector2 {
                    x: -scale_start * delta.y,
                    y: scale_start * delta.x,
                };
                let radius_end = Vector2 {
                    x: -scale_end * delta.y,
                    y: scale_end * delta.x,
                };

                let strip = [
                    Vector2::new(
                        prev_point.point.x - radius_start.x,
                        prev_point.point.y - radius_start.y,
                    ),
                    Vector2::new(
                        prev_point.point.x + radius_start.x,
                        prev_point.point.y + radius_start.y,
                    ),
                    Vector2::new(point.point.x - radius_end.x, point.point.y - radius_end.y),
                    Vector2::new(point.point.x + radius_end.x, point.point.y + radius_end.y),
                ];
                draw.draw_triangle_strip(&strip, color);
                if circles && (thick_end / 2.0) * zoom > 2.0 {
                    draw.draw_circle_v(point.point, thick_end / 2.0, color);
                }
            }

            if circles && i == 1 {
                draw.draw_circle_v(prev_point.point, thick_start / 2.0, color);
            }

            prev_point = point;
        }
    }
}
