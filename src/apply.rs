use crate::Turtle;
use raylib::core::math::Vector2;
use std::collections::HashMap;
use std::str::FromStr;
use thiserror::Error;

#[derive(Clone, Debug)]
pub enum Command {
    Save,
    Load,
    Forwards(f32),
    GhostForwards(f32),
    SetHeading(f32),
    Rotate(f32),
    Change(ChangeOp),
    Eq(f32),
    PenUp,
    PenDown,
    PenToggle,
    Two(Box<Command>, Box<Command>),
}

impl Command {
    pub fn get_value_mut(&mut self) -> Option<&mut f32> {
        match self {
            Self::Forwards(ref mut x) => Some(x),
            Self::GhostForwards(ref mut x) => Some(x),
            Self::SetHeading(ref mut x) => Some(x),
            Self::Rotate(ref mut x) => Some(x),
            Self::Eq(ref mut x) => Some(x),
            _ => None,
        }
    }

    pub fn get_value(&self) -> Option<f32> {
        match self {
            Self::Forwards(x) => Some(*x),
            Self::GhostForwards(x) => Some(*x),
            Self::SetHeading(x) => Some(*x),
            Self::Rotate(x) => Some(*x),
            Self::Eq(x) => Some(*x),
            _ => None,
        }
    }

    pub fn execute<H: std::hash::BuildHasher + Clone>(
        &self,
        turtle: &mut Turtle,
        characters: &mut HashMap<char, Command, H>,
        point_stack: &mut Vec<(Vector2, f32, HashMap<char, Command, H>, f32)>,
        my_char: &char,
    ) -> Result<(), crate::Error> {
        match self {
            Command::Save => {
                characters.insert(*my_char, self.clone());
                point_stack.push((
                    turtle.get_position(),
                    turtle.heading,
                    characters.clone(),
                    turtle.thick,
                ))
            }
            Command::Load => {
                let (pos, heading, loaded_characters, thick) =
                    point_stack.pop().ok_or(crate::Error::NoSavedPoint)?;
                turtle.thick = thick;
                *characters = loaded_characters;
                turtle.pen_down = false;
                turtle.goto(pos);
                turtle.heading = heading;
                turtle.pen_down = true;
            }
            Command::GhostForwards(amount) => {
                let pen = turtle.pen_down;
                turtle.pen_down = false;
                turtle.forward(*amount);
                turtle.pen_down = pen;
            }
            Command::Forwards(amount) => turtle.forward(*amount),
            Command::SetHeading(amount) => turtle.heading = *amount,
            Command::Rotate(amount) => turtle.rotate(*amount),
            Command::Change(change) => {
                let var1 = change.var1.realise(characters);
                let var2 = change.var2.realise(characters);
                let operation = change.operation;
                let assign_v = change.assign;

                let assign = characters
                    .get_mut(&assign_v)
                    .and_then(|x| x.get_value_mut());
                if let Some(assign) = assign {
                    match operation {
                        Operation::Add => *assign = var1 + var2,
                        Operation::Mul => *assign = var1 * var2,
                        Operation::Div => *assign = var1 / var2,
                        Operation::Sub => *assign = var1 - var2,
                        Operation::Rem => *assign = var1 % var2,
                    }
                    if assign_v == '~' {
                        turtle.thick = *assign;
                    }
                }
            }
            Command::PenUp => turtle.pen_down = false,
            Command::PenDown => turtle.pen_down = true,
            Command::PenToggle => turtle.pen_down ^= true,
            Command::Eq(_) => (),
            Command::Two(cmd1, cmd2) => {
                cmd1.execute(turtle, characters, point_stack, my_char)?;
                cmd2.execute(turtle, characters, point_stack, my_char)?;
            }
        }

        Ok(())
    }
}

#[derive(Clone, Debug, Copy)]
pub struct ChangeOp {
    pub assign: char,
    pub operation: Operation,
    pub var1: MVar,
    pub var2: MVar,
}

#[derive(Clone, Debug, Copy)]
pub enum MVar {
    Constant(f32),
    Dynamic(char),
}

impl MVar {
    pub fn realise(&self, characters: &HashMap<char, Command, impl std::hash::BuildHasher>) -> f32 {
        match self {
            Self::Constant(x) => *x,
            Self::Dynamic(x) => characters.get(x).and_then(|x| x.get_value()).unwrap_or(0.0),
        }
    }
}

impl FromStr for MVar {
    type Err = CommandError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.parse() {
            Ok(x) => Ok(Self::Constant(x)),
            Err(_) => Ok(Self::Dynamic(
                s.chars().next().ok_or(Self::Err::NoVariable)?,
            )),
        }
    }
}

#[derive(Clone, Debug, Copy)]
pub enum Operation {
    Add,
    Mul,
    Div,
    Sub,
    Rem,
}

#[derive(Error, Debug)]
pub enum CommandError {
    #[error("invalid command")]
    InvalidCommand,
    #[error("invalid float: {0}")]
    ParseFloatError(#[from] std::num::ParseFloatError),
    #[error("missing arg")]
    MissingArg,
    #[error("no variable given in mathematical expression")]
    NoVariable,
    #[error("missing operand in mathematical expression")]
    MissingOperand,
    #[error("invalid operand in mathematical expression")]
    InvalidOperand,
    #[error("missing assignation in mathematical expression")]
    MissingAssignation,
}

impl FromStr for Command {
    type Err = CommandError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some((cmd1, cmd2)) = s.split_once(';') {
            return Ok(Self::Two(Box::new(cmd1.parse()?), Box::new(cmd2.parse()?)));
        }

        if s.starts_with('{') {
            Ok(Self::Save)
        } else if s.starts_with('}') {
            Ok(Self::Load)
        } else if let Some(s) = s.strip_prefix(">%") {
            Ok(Self::GhostForwards(s.parse()?))
        } else if let Some(s) = s.strip_prefix('>') {
            Ok(Self::Forwards(s.parse()?))
        } else if let Some(s) = s.strip_prefix("*@") {
            Ok(Self::SetHeading(s.parse()?))
        } else if let Some(s) = s.strip_prefix('*') {
            Ok(Self::Rotate(s.parse()?))
        } else if let Some(s) = s.strip_prefix('$') {
            let operands = ['+', '*', '-', '/', '%'];
            let (assign, expression) = s.split_once('=').ok_or(Self::Err::MissingAssignation)?;
            let (var1, var2) = expression
                .split_once(|c: char| operands.contains(&c))
                .ok_or(Self::Err::MissingOperand)?;
            let operand = s
                .chars()
                .find(|c| operands.contains(c))
                .ok_or(Self::Err::InvalidOperand)?;
            Ok(Self::Change(ChangeOp {
                assign: assign.chars().next().ok_or(Self::Err::MissingAssignation)?,
                operation: match operand {
                    '+' => Operation::Add,
                    '*' => Operation::Mul,
                    '/' => Operation::Div,
                    '-' => Operation::Sub,
                    '%' => Operation::Rem,
                    _ => return Err(Self::Err::InvalidOperand),
                },
                var1: var1.parse()?,
                var2: var2.parse()?,
            }))
        } else if s.starts_with('^') {
            Ok(Self::PenUp)
        } else if s.starts_with('v') {
            Ok(Self::PenDown)
        } else if s.starts_with('%') {
            Ok(Self::PenToggle)
        } else if let Some(s) = s.strip_prefix('=') {
            Ok(Self::Eq(s.parse()?))
        } else {
            Err(Self::Err::InvalidCommand)
        }
    }
}
