use raylib::{
    prelude::{Color, MouseButton, RaylibDraw, RaylibDrawHandle, Rectangle},
    text::measure_text_ex,
    RaylibHandle,
};

pub struct Button {
    pub rec: Rectangle,
    pub colour: Color,
    pub text: String,
    pub text_colour: Color,
}

impl Button {
    pub fn new(rec: Rectangle, text: String, colour: Color, text_colour: Color) -> Button {
        Button {
            rec: rec,
            text: text,
            colour: colour,
            text_colour: text_colour,
        }
    }

    pub fn was_clicked(&mut self, rl: &RaylibHandle) -> bool {
        self.rec.check_collision_point_rec(rl.get_mouse_position())
            && rl.is_mouse_button_pressed(MouseButton::MOUSE_LEFT_BUTTON)
    }

    pub fn draw_button(&self, d: &mut RaylibDrawHandle) {
        d.draw_rectangle(
            self.rec.x as i32,
            self.rec.y as i32,
            self.rec.width as i32,
            self.rec.height as i32,
            self.colour,
        );

        // d.draw_text(
        //     &self.text,
        //     (measure_text_ex(d.get_font_default(), &self.text, 15.0, 0.0).x
        //         + self.rec.x
        //         + (self.rec.width - measure_text_ex(d.get_font_default(), &self.text, 15.0, 0.0).x) / 2.0) as i32,
        //     // (self.rec.x + self.rec.width / 5.0) as i32,
        //     (measure_text_ex(d.get_font_default(), &self.text, 15.0, 0.0).y
        //         + self.rec.y
        //         + (self.rec.width - measure_text_ex(d.get_font_default(), &self.text, 15.0, 0.0).y) / 2.0) as i32,
        //     // (self.rec.y + self.rec.height / 6.0) as i32,
        //     15,
        //     self.text_colour,
        // );
        let measure = measure_text_ex(d.get_font_default(), &self.text, 15.0, 0.0);

        d.draw_text(
            &self.text,
            centre(measure.x, self.rec.x, self.rec.x + self.rec.width).round() as i32,
            centre(measure.y, self.rec.y, self.rec.y + self.rec.height).round() as i32,
            15,
            self.text_colour,
        )
    }
}

fn centre (size: f32, start: f32, end: f32) -> f32{
    let centre = (start + end) / 2.0;
    centre - size / 2.0
}


