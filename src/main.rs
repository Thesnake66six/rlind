mod turtle;
pub use apply::{Command, Operation};
mod apply;
mod button;
mod hash;

use button::Button;
use clap::Parser;
use raylib::prelude::*;
use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::mpsc::{self, Receiver, SyncSender};
use std::thread::{self};
use std::{fs, io};
use thiserror::Error;
use turtle::Turtle;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Invalid characters ':' line")]
    InvalidCharactersLine,
    #[error("Invalid rules '->' line")]
    InvalidRulesLine,
    #[error("Bad character definition")]
    BadCharacterDefinition,
    #[error("No point to load")]
    NoSavedPoint,
    #[error("IO Error: {0}")]
    Io(#[from] io::Error),
}

#[derive(Parser)]
pub struct Args {
    /// The path to the lind file
    pub path: PathBuf,
    /// Should the axiom be printed at the end of computation
    #[clap(long, short)]
    pub print_axiom: bool,
}

const CAMERA_MOVE_SPEED: f32 = -1.0;
const CAMERA_SCROLL_SPEED: f32 = 0.1;
const CAMERA_ZOOM: f32 = 0.5;
const TURTLE_THICK: f32 = 25.0;
// Defining some commonly-used colours
const RB_BG: Color = Color {
    r: 20,
    g: 20,
    b: 20,
    a: 255,
};
const RB_TXT: Color = Color {
    r: 80,
    g: 80,
    b: 80,
    a: 255,
};
const RB_BTN: Color = Color {
    r: 160,
    g: 160,
    b: 160,
    a: 255,
};
const BW_BG: Color = Color {
    r: 200,
    g: 200,
    b: 200,
    a: 255,
};
const BW_TXT: Color = Color {
    r: 20,
    g: 20,
    b: 20,
    a: 255,
};
const BW_BTN: Color = Color {
    r: 160,
    g: 160,
    b: 160,
    a: 255,
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let (tx, rx): (SyncSender<Turtle>, Receiver<Turtle>) = mpsc::sync_channel(0);

    thread::spawn(move || {
        let args = Args::parse();
        let lind_def = fs::read_to_string(&args.path)?;
        let mut current_turtle = Turtle::new(TURTLE_THICK);

        let mut characters: HashMap<char, Command, hash::CustomHasher> =
            HashMap::with_hasher(hash::CustomHasher::default());

        let mut rules = HashMap::with_hasher(hash::CustomHasher::default());
        let mut axiom = String::new();

        eprintln!("Evaluating axiom...");
        for line in lind_def.split('\n') {
            let line = line.replace(' ', "");
            if line.starts_with('#') {
                continue;
            } else if line.chars().nth(1) == Some(':') {
                let (chr, meaning) = line
                    .split_once(':')
                    .ok_or(Error::InvalidCharactersLine)
                    .unwrap();
                characters.insert(chr.chars().next().unwrap_or('#'), meaning.parse().unwrap());
            } else if line.contains("->") {
                let (chr, mut expanse) = line
                    .split_once("->")
                    .ok_or(Error::InvalidRulesLine)
                    .unwrap();
                if expanse == "#" {
                    expanse = "";
                }
                rules.insert(chr.chars().next().unwrap_or('#'), expanse.to_string());
            } else if line.starts_with('"') || line.starts_with('\'') {
                axiom = line.replace('\'', "").replace('"', "");
            }
        }

        let mut iteration = 0;
        let og_characters = characters.clone();

        loop {
            characters = og_characters.clone();
            eprintln!("Running iteration {}.", iteration);

            let mut new_axiom = String::new();

            for chr in axiom.chars() {
                match rules.get(&chr) {
                    Some(rule) => new_axiom.push_str(rule),
                    None => new_axiom.push(chr),
                }
            }

            axiom = new_axiom;

            eprintln!("Evaluated axiom.");
            if args.print_axiom {
                println!("{}", axiom);
            }

            let mut point_stack = Vec::new();

            if let Some(pen_width) = characters.get(&'~') {
                current_turtle.thick = pen_width.get_value().unwrap_or(TURTLE_THICK);
            }

            eprintln!("Running the turtle...");
            for chr in axiom.chars() {
                if let Some(command) = characters.remove(&chr) {
                    command
                        .execute(&mut current_turtle, &mut characters, &mut point_stack, &chr)
                        .unwrap();
                    characters.insert(chr, command);
                }
            }
            eprintln!("Sending iteration {}", iteration);
            tx.send(current_turtle).unwrap();
            eprintln!("Transmitted iteration {}", iteration);
            iteration += 1;
            current_turtle = Turtle::new(TURTLE_THICK);
        }
        #[allow(unreachable_code)]
        Ok::<(), Error>(())
    });

    // Pre-draw logic
    let mut rainbow = false;
    let mut circles = false;

    let mut iteration = 0_usize;
    let mut turtle_list = Vec::new();

    turtle_list.push(rx.recv().unwrap());

    // Initialization
    //--------------------------------------------------------------------------------------

    let screen_width = 800;
    let screen_height = 450;
    let (mut rl, thread) = raylib::init()
        .size(screen_width, screen_height)
        .title("Rlind")
        .resizable()
        .build();

    let mut points_rect = turtle_list[iteration].points_rect();
    let mut size = (points_rect.width * points_rect.height).sqrt();

    let mut camera = Camera2D::default();
    let mut mouse_prev: Vector2 = Vector2 {
        x: (0.0f32),
        y: (0.0f32),
    };
    // Centre
    camera.target = Vector2 {
        x: points_rect.x + points_rect.width / 2.0f32,
        y: points_rect.y + points_rect.height / 2.0f32,
    };
    camera.zoom = f32::max(
        rl.get_screen_width() as f32 / points_rect.width * CAMERA_ZOOM,
        rl.get_screen_height() as f32 / points_rect.height * CAMERA_ZOOM,
    );

    let mut prev_iteration_buttton = Button::new(
        Rectangle::new(10.0, 35.0, 15.0, 15.0),
        "-".to_string(),
        BW_BTN,
        BW_TXT,
    );
    let mut next_iteration_buttton = Button::new(
        Rectangle::new(35.0, 35.0, 15.0, 15.0),
        "+".to_string(),
        BW_BTN,
        BW_TXT,
    );

    //--------------------------------------------------------------------------------------

    rl.set_target_fps(60);
    // Main game loop
    while !rl.window_should_close() {
        let mut changed_iteration = false;

        // Thread logic
        if iteration + 5 > turtle_list.len() {
            match rx.try_recv() {
                Ok(msg) => {
                    turtle_list.push(msg);
                }
                Err(_) => (),
            };
        }

        // Centre the camera
        camera.offset = Vector2 {
            x: rl.get_screen_width() as f32 / 2.0f32,
            y: rl.get_screen_height() as f32 / 2.0f32,
        };

        // Increment the zoom based of the mousewheel
        camera.zoom += rl.get_mouse_wheel_move() * CAMERA_SCROLL_SPEED * camera.zoom;

        // Toggling rainbow and circles when the R and C keys are pressed respectively.
        if rl.is_key_pressed(KeyboardKey::KEY_R) {
            rainbow ^= true;
            if rainbow {
                prev_iteration_buttton.colour = RB_BTN;
                prev_iteration_buttton.text_colour = RB_TXT;
                next_iteration_buttton.colour = RB_BTN;
                next_iteration_buttton.text_colour = RB_TXT;
            } else {
                prev_iteration_buttton.colour = BW_BTN;
                prev_iteration_buttton.text_colour = BW_TXT;
                next_iteration_buttton.colour = BW_BTN;
                next_iteration_buttton.text_colour = BW_TXT;
            }
        }

        if rl.is_key_pressed(KeyboardKey::KEY_C) {
            circles ^= true;
        }

        //Change current iteration when arrows are pressed
        if rl.is_key_pressed(KeyboardKey::KEY_UP) || next_iteration_buttton.was_clicked(&rl) {
            changed_iteration = true;
            iteration += 1;

            if iteration > turtle_list.len() - 1 {
                iteration -= 1;
            }
        }

        if rl.is_key_pressed(KeyboardKey::KEY_DOWN) || prev_iteration_buttton.was_clicked(&rl) {
            changed_iteration = true;
            iteration = iteration.saturating_sub(1);
        }

        if changed_iteration {
            let new_points_rect = turtle_list[iteration].points_rect();
            let new_size = (new_points_rect.width * new_points_rect.height).sqrt();

            camera.target = ((camera.target - Vector2::new(points_rect.x, points_rect.y))
                * (new_size / size))
                + Vector2::new(new_points_rect.x, new_points_rect.y);
            camera.zoom *= size / new_size;
            size = new_size;
            points_rect = new_points_rect;
        }

        //Re-centre camera when Enter pressed
        if rl.is_key_pressed(KeyboardKey::KEY_ENTER) {
            camera.target = Vector2 {
                x: points_rect.x + (points_rect.width / 2.0f32),
                y: points_rect.y + (points_rect.height / 2.0f32),
            };
            camera.zoom = f32::max(
                rl.get_screen_width() as f32 / points_rect.width * CAMERA_ZOOM,
                rl.get_screen_height() as f32 / points_rect.height * CAMERA_ZOOM,
            );
        }

        // Prevent zoom from being negative
        if camera.zoom < 0.0 {
            camera.zoom *= -1.0;
        }

        // Support panning
        if rl.is_mouse_button_down(MouseButton::MOUSE_LEFT_BUTTON) {
            let offset = rl.get_mouse_position();
            camera.target.x += (offset.x - mouse_prev.x) * CAMERA_MOVE_SPEED / camera.zoom;
            camera.target.y += (offset.y - mouse_prev.y) * CAMERA_MOVE_SPEED / camera.zoom;
            mouse_prev = offset;
        } else {
            mouse_prev = rl.get_mouse_position();
        }

        // Get the smallest and largest positions
        // This allows the turtle to only draw the necassary
        // lines.
        let camera_min = rl.get_screen_to_world2D(Vector2::zero(), camera);
        let camera_max = rl.get_screen_to_world2D(
            Vector2 {
                x: rl.get_screen_width() as f32,
                y: rl.get_screen_height() as f32,
            },
            camera,
        );

        // Draw
        //----------------------------------------------------------------------------------
        let mut d = rl.begin_drawing(&thread);
        let mut draw = d.begin_mode2D(camera);
        if rainbow {
            draw.clear_background(RB_BG);
        } else {
            draw.clear_background(BW_BG);
        }
        turtle_list[iteration].draw_with_limits(
            camera_min,
            camera_max,
            camera.zoom,
            &mut draw,
            rainbow,
            circles,
        );
        drop(draw);

        // Draw text elements to screen
        d.draw_text(
            format!("Iteration {} of {}", iteration + 1, turtle_list.len()).as_str(),
            10,
            10,
            15,
            if rainbow { RB_TXT } else { BW_TXT },
        );

        prev_iteration_buttton.draw_button(&mut d);
        next_iteration_buttton.draw_button(&mut d);
    }
    Ok(())
}
